/****************************************************************************
** Meta object code from reading C++ file 'feedback.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.6)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../JoystickFeedback/feedback.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'feedback.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.6. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_Feedback[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      10,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: signature, parameters, type, tag, flags
      10,    9,    9,    9, 0x05,
      26,    9,    9,    9, 0x05,

 // slots: signature, parameters, type, tag, flags
      58,   42,    9,    9, 0x0a,
      82,    9,    9,    9, 0x0a,
      94,    9,    9,    9, 0x0a,
     112,  108,    9,    9, 0x0a,
     136,    9,    9,    9, 0x0a,
     151,    9,    9,    9, 0x0a,
     177,  167,    9,    9, 0x0a,
     193,    9,    9,    9, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_Feedback[] = {
    "Feedback\0\0targetReached()\0feedbackError()\0"
    "TargetNum,Angle\0createScene(int,double)\0"
    "setCursor()\0setReadyCue()\0x,y\0"
    "setBallPos(float,float)\0startPolling()\0"
    "updateDisplay()\0isReached\0stopTrial(bool)\0"
    "trialInterrupted()\0"
};

void Feedback::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        Feedback *_t = static_cast<Feedback *>(_o);
        switch (_id) {
        case 0: _t->targetReached(); break;
        case 1: _t->feedbackError(); break;
        case 2: _t->createScene((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< double(*)>(_a[2]))); break;
        case 3: _t->setCursor(); break;
        case 4: _t->setReadyCue(); break;
        case 5: _t->setBallPos((*reinterpret_cast< float(*)>(_a[1])),(*reinterpret_cast< float(*)>(_a[2]))); break;
        case 6: _t->startPolling(); break;
        case 7: _t->updateDisplay(); break;
        case 8: _t->stopTrial((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 9: _t->trialInterrupted(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData Feedback::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject Feedback::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_Feedback,
      qt_meta_data_Feedback, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &Feedback::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *Feedback::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *Feedback::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_Feedback))
        return static_cast<void*>(const_cast< Feedback*>(this));
    return QObject::qt_metacast(_clname);
}

int Feedback::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 10)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 10;
    }
    return _id;
}

// SIGNAL 0
void Feedback::targetReached()
{
    QMetaObject::activate(this, &staticMetaObject, 0, 0);
}

// SIGNAL 1
void Feedback::feedbackError()
{
    QMetaObject::activate(this, &staticMetaObject, 1, 0);
}
QT_END_MOC_NAMESPACE
