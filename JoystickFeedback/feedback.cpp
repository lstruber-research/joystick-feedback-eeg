#include "feedback.h"

using namespace std;

int TARGET_NUMBER;
int TARGET_DISTANCE;
int TARGET_SIZE;
int BALL_SIZE;

int CONSTANT_DEV;
int RDM_MAX_DEV;
int RDM_STEP_DEV;

int RESTING_TIME;
int WHITE_SCREEN_TIME;
int CURSOR_MIN_TIME;
int CURSOR_MAX_TIME;
int READY_CUE_MIN_TIME;
int READY_CUE_MAX_TIME;
int TRIAL_MAX_TIME;

int DATA_RATE;
int DISPLAY_RATE = 60;
int TIME_TO_VALID_TARGET = 200;
int BOARD_NUM;

vector<int> PossibleDevAngles;
vector<int> TargetList;
float JoyXCalib, JoyYCalib;

Feedback::Feedback(QGraphicsView &view, char* dataFolder, QString username, int fileNumber, int phase, int trigRadius) : iView(view), iPhase(phase), iTrigRadius(trigRadius)
{
    sceneOnceCreated = false;
    iScene = new QGraphicsScene((QObject*)&iView);
    iBlackScene = new QGraphicsScene((QObject*)&iView);
    updateDisplayTimer = new QTimer(this);
    updateDisplayTimer->setInterval((int) 1000/DISPLAY_RATE);
    connect(updateDisplayTimer, SIGNAL(timeout()), this, SLOT(updateDisplay()));

    fError = false;
    initCanceled = false;

    char fileNumberChar[4] ;
    sprintf_s(fileNumberChar, "%d", fileNumber + 1);

    Rate = (long) DATA_RATE;
    Count = ((long)TRIAL_MAX_TIME)/1000 * Rate * 3; // Seconds * Samples/sec * NbChannels

    BufferData = new WORD[Count];

    iTargets.resize(TARGET_NUMBER);

    // Create file for data saving
    switch(iPhase)
    {
        case 1:
        {
            strcpy_s(fileName, dataFolder);
            strcat_s(fileName, "\\RAW_F_");
            strcat_s(fileName, fileNumberChar);
            strcat_s(fileName, "_");
            strcat_s(fileName, username.toStdString().c_str());
            strcat_s(fileName, ".csv");
            break;
        }
        case 2:
            strcpy_s(fileName, dataFolder);
            strcat_s(fileName, "\\RAW_J_");
            strcat_s(fileName, fileNumberChar);
            strcat_s(fileName, "_");
            strcat_s(fileName, username.toStdString().c_str());
            strcat_s(fileName, ".csv");
            break;
        case 3:
        {
            strcpy_s(fileName, dataFolder);
            strcat_s(fileName, "\\RAW_R_");
            strcat_s(fileName, fileNumberChar);
            strcat_s(fileName, "_");
            strcat_s(fileName, username.toStdString().c_str());
            strcat_s(fileName, ".csv");
            break;
        }
        case 4:
        {
            strcpy_s(fileName, dataFolder);
            strcat_s(fileName, "\\RAW_RJ_");
            strcat_s(fileName, fileNumberChar);
            strcat_s(fileName, "_");
            strcat_s(fileName, username.toStdString().c_str());
            strcat_s(fileName, ".csv");
            break;
        }
        case 5:
        {
            strcpy_s(fileName, dataFolder);
            strcat_s(fileName, "\\RAW_RR_");
            strcat_s(fileName, fileNumberChar);
            strcat_s(fileName, "_");
            strcat_s(fileName, username.toStdString().c_str());
            strcat_s(fileName, ".csv");
            break;
        }
    }

    dataFile.open(fileName);
    dataFile.clear();
    dataFile.close();

}

bool Feedback::isInitCanceled()
{
    return initCanceled;
}

void Feedback::createScene(int TargetNum, double Angle)
{
    iBlackScene->setBackgroundBrush(QBrush(Qt::white));
    iView.setScene(iBlackScene);

    iScene->clear();
    iTargetNum = TargetNum;
    dAngle = Angle;

    dStartAngularDev = 0;
    dRealAngularDev = dStartAngularDev;
    eventLaunched = false;

    if(iTargetNum >= TARGET_NUMBER)
        iTargetNum = TARGET_NUMBER - 1;

    for(int i = 0; i < TARGET_NUMBER; i++)
    {
        int targetX = TARGET_DISTANCE*cos(i*2*PI/TARGET_NUMBER);
        int targetY = TARGET_DISTANCE*sin(i*2*PI/TARGET_NUMBER);
        iTargets[i] = new QGraphicsEllipseItem(- TARGET_SIZE/2, - TARGET_SIZE/2,TARGET_SIZE,TARGET_SIZE);
        iTargets[i]->setPos(targetX, targetY);
        iTargets[i]->setBrush(QBrush(Qt::transparent));
        iTargets[i]->setPen(QPen(Qt::transparent));
        iScene->addItem(iTargets[i]);
    }
    startTargetXCenter = TARGET_DISTANCE*cos(iTargetNum*2*PI/TARGET_NUMBER);
    startTargetYCenter = TARGET_DISTANCE*sin(iTargetNum*2*PI/TARGET_NUMBER);
    realTargetXCenter = startTargetXCenter;
    realTargetYCenter = startTargetYCenter;

    iBall = new QGraphicsEllipseItem(-BALL_SIZE/2, -BALL_SIZE/2, BALL_SIZE, BALL_SIZE);
    iBall->setBrush(QBrush(Qt::green));
    iBall->setPen(QPen(Qt::transparent));
    ballXPos.clear();
    ballYPos.clear();
    TrigVector.clear();
    iScene->addItem(iBall);
    BallXCenter_old = 0;
    BallYCenter_old = 0;
    JoystickX_old = 0;
    JoystickY_old = 0;

    targetTimes = 0;

    sceneOnceCreated = true;
}

void Feedback::setCursor()
{
    iView.setScene(iScene);
}

void Feedback::setReadyCue()
{
    for(int i = 0; i < TARGET_NUMBER; i++)
        iTargets[i]->setBrush(QBrush(Qt::gray));
}

void Feedback::setBallPos(float x, float y){
    iBall->setPos(x,y);
    QGraphicsLineItem *traj = iScene->addLine(BallXCenter_old,BallYCenter_old,x,y);
    traj->setPen(QPen(Qt::green,5));
}

void Feedback::startPolling(){
    for(int i = 0; i < TARGET_NUMBER; i++)
        iTargets[i]->setBrush(QBrush(Qt::transparent));
    iTargets[iTargetNum]->setBrush(QBrush(Qt::red));

    unsigned Options = NOCONVERTDATA + BACKGROUND + SINGLEIO;

    // cbAInScan(BoardNumber, LowChannel, HighChannel, Count, &Rate, Gain, Buffer, Options);
    int ULStat = cbAInScan (BOARD_NUM, 0, 2, Count, &Rate, BIP5VOLTS, BufferData, Options);
    if(ULStat != 0)
    {
        // ERROR !
        emit feedbackError();
        fError = true;
        return;
    }

    ULStat = cbDBitOut(BOARD_NUM, AUXPORT, 0, 1);
    if(ULStat != 0)
    {
        // ERROR !
        emit feedbackError();
        fError = true;
        return;
    }

    updateDisplayTimer->start();
}

void Feedback::startEvent() {
    if(iPhase == 2 || iPhase == 4)
    {
        double newTargetAngle = iTargetNum*2*PI/TARGET_NUMBER + dAngle;

        realTargetXCenter = TARGET_DISTANCE*cos(newTargetAngle);
        realTargetYCenter = TARGET_DISTANCE*sin(newTargetAngle);
        iTargets[iTargetNum]->setPos(realTargetXCenter,realTargetYCenter);
        cout << "Angle : " << 180/PI*iTargetNum*2*PI/TARGET_NUMBER << endl;
        cout << "newAngle : " << 180/PI*newTargetAngle << endl;
    }
    else if(iPhase == 3 || iPhase == 5)
        dRealAngularDev = dAngle;
}

void Feedback::updateDisplay()
{
    //cout << "upd" << endl;
    short Status = 0;
    long CurCount, CurIndex;
    int ULStat = cbGetStatus(BOARD_NUM, &Status, &CurCount, &CurIndex, AIFUNCTION);
    //cout << "getstatusok" << endl;
    if(ULStat != 0) {
        cout << "error :" << ULStat << endl;
        emit feedbackError();
        fError = true;
        updateDisplayTimer->stop();
        return;
    }

    //cout << CurIndex << " / " << CurCount << endl;
    // cout << (float)BufferData[CurIndex - 1] << "\t" << (float)BufferData[CurIndex] << endl;
    float XVolt = ((float)BufferData[CurIndex + 1]) * 10/65535 - 5;
    float YVolt = ((float)BufferData[CurIndex]) * 10/65535 - 5;
    // cout << XVolt << "\t" << YVolt << endl;
    // Asservissement position
    float JoystickX = -((((float)TARGET_DISTANCE) + 100)/2 * (XVolt - JoyXCalib));
    float JoystickY = -((((float)TARGET_DISTANCE) + 100)/2 * (YVolt - JoyYCalib));
    // cout << JoystickX << "\t" << JoystickY << endl;

    float BallXCenter, BallYCenter;

    //cout << "BallCenter : " << JoystickX << "\t" << JoystickY << endl;
    float DecX = JoystickX - JoystickX_old;
    float DecY = JoystickY - JoystickY_old;
    /*cout << "dRealAngularDev : "<< dRealAngularDev << endl;
    cout << "Dec : " << DecX << "\t" << DecY << endl;
    cout << "Calib : " << JoyXCalib << "\t" << JoyYCalib << endl;
    cout << "dRealAngularDev : "<< dRealAngularDev << endl;*/
    BallXCenter = BallXCenter_old + DecX * cos(dRealAngularDev) + DecY * sin(dRealAngularDev);
    BallYCenter = BallYCenter_old + DecY * cos(dRealAngularDev) - DecX * sin(dRealAngularDev);
    //cout << "BallCenterMod : "<< BallXCenter << "\t" << BallYCenter << endl;
    JoystickX_old = JoystickX;
    JoystickY_old = JoystickY;

    //cout << "BallCenter : "<< BallXCenter << "\t" << BallYCenter << endl;
    setBallPos(BallXCenter,BallYCenter);

    if(!eventLaunched && (sqrt(pow(BallXCenter,2) + pow(BallYCenter,2)) > iTrigRadius)) {
        eventLaunched = true;
        startEvent();
    }

    iTargets[iTargetNum]->setBrush(QBrush(Qt::red));
    //cout << "colored" << endl;
    int dist = BalltoTargetDist(BallXCenter, BallYCenter);
    BallXCenter_old = BallXCenter;
    BallYCenter_old = BallYCenter;

    if((dist < (TARGET_SIZE - BALL_SIZE)/2))
    {
        targetTimes++;
        double targetTime = 1000/DISPLAY_RATE * targetTimes;
        if(targetTime > TIME_TO_VALID_TARGET)
            emit targetReached();
    }
    else {
        targetTimes = 0;
    }
}
void Feedback::trialInterrupted()
{
    int ULStat = cbDBitOut(BOARD_NUM, AUXPORT, 0, 0);
    if(ULStat != 0)
    {
        // ERROR !
        emit feedbackError();
        fError = true;
    }

    short Status = 0;
    long CurCount, CurIndex;
    ULStat = cbGetStatus(BOARD_NUM, &Status, &CurCount, &CurIndex, AIFUNCTION);
    if(ULStat != 0)
    {
        emit feedbackError();
        fError = true;
    }
    else
    {
        if(Status != 0) ULStat = cbStopBackground(BOARD_NUM,AIFUNCTION);
        if(ULStat != 0)
        {
            emit feedbackError();
            fError = true;
        }
        updateDisplayTimer->stop();
    }
}

void Feedback::stopTrial(bool isReached)
{
    short Status = 0;
    long CurCount, CurIndex;

    updateDisplayTimer->stop();
    Sleep(100);

    int ULStat = cbGetStatus(BOARD_NUM, &Status, &CurCount, &CurIndex, AIFUNCTION);
    if(ULStat != 0)
    {
        emit feedbackError();
        fError = true;
        return;
    }

    ULStat = cbDBitOut(BOARD_NUM, AUXPORT, 0, 0);
    if(ULStat != 0)
    {
        // ERROR !
        emit feedbackError();
        fError = true;
        return;
    }

    ULStat = cbStopBackground(BOARD_NUM,AIFUNCTION);
    if(ULStat != 0)
    {
        emit feedbackError();
        fError = true;
        return;
    }

    long Rate = (long) DATA_RATE;
    if(Status == 0) CurCount = ((long)TRIAL_MAX_TIME)/1000 * Rate * 3;

    cout << "computation" << endl;
    JoystickX_old = 0;
    JoystickY_old = 0;
    BallXCenter_old = 0;
    BallYCenter_old = 0;
    bool eventStart = false;
    for(int i = 0; i < CurCount; i = i + 3)
    {
        float XVolt = ((float)BufferData[i+1]) * 10/65535 - 5;
        float YVolt = ((float)BufferData[i]) * 10/65535 - 5;
        float Trig = ((float)BufferData[i+2]) * 10/65535 - 5;

        float JoystickX = -((((float)TARGET_DISTANCE) + 100)/2 * (XVolt - JoyXCalib));
        float JoystickY = -((((float)TARGET_DISTANCE) + 100)/2 * (YVolt - JoyYCalib));

        float BallXCenter, BallYCenter;
        float DecX = JoystickX - JoystickX_old;
        float DecY = JoystickY - JoystickY_old;

        /*double idx = ((double) i)/3;
        if(idx/Rate*1000 < eventTime)*/

        if(!eventStart && (sqrt(pow(BallXCenter_old,2) + pow(BallYCenter_old,2)) > iTrigRadius))
            eventStart = true;

        if(eventStart == true) {
            BallXCenter = BallXCenter_old + DecX * cos(dStartAngularDev) + DecY * sin(dStartAngularDev);
            BallYCenter = BallYCenter_old + DecY * cos(dStartAngularDev) - DecX * sin(dStartAngularDev);
        }
        else {
            BallXCenter = BallXCenter_old + DecX * cos(dRealAngularDev) + DecY * sin(dRealAngularDev);
            BallYCenter = BallYCenter_old + DecY * cos(dRealAngularDev) - DecX * sin(dRealAngularDev);
        }

        ballXPos.push_back((int)BallXCenter);
        ballYPos.push_back((int)-BallYCenter);
        if(Trig > 2.5)
            TrigVector.push_back(1);
        else
            TrigVector.push_back(0);

        JoystickX_old = JoystickX;
        JoystickY_old = JoystickY;
        BallXCenter_old = BallXCenter;
        BallYCenter_old = BallYCenter;
    }

    cout << "saving" << endl;
    dataFile.open(fileName, fstream::out | fstream::app);
    ostream_iterator<int> output_iterator(dataFile,";");
    dataFile << (int) (dAngle*180/PI) << ";";
    dataFile << startTargetXCenter << ";";
    copy(ballXPos.begin(), ballXPos.end(), output_iterator);
    dataFile << "\n";
    dataFile << " ;";
    dataFile << -startTargetYCenter << ";";
    copy(ballYPos.begin(), ballYPos.end(), output_iterator);
    dataFile << "\n";
    dataFile<< " ; ;";
    copy(TrigVector.begin(), TrigVector.end(), output_iterator);
    dataFile << "\n";
    dataFile.close();
    dataFile.clear();
    /*if(isReached)
        cout << "Bravo" << endl;*/
}

int Feedback::BalltoTargetDist(float ballXCenter, float ballYCenter) {
   return (int) sqrt(pow(ballXCenter - ((float)realTargetXCenter),2) + pow(ballYCenter - ((float)realTargetYCenter),2));
}

Feedback::~Feedback()
{
    if(sceneOnceCreated == true)
    {
        cout << "destroying targets" << endl;
        for(int i = 0; i < TARGET_NUMBER; i++)
            if(iTargets[i]) delete(iTargets[i]);
        iTargets.clear();
        cout << "destroying ball" << endl;
        if(iBall)
            delete(iBall);
    }

    cout << "destroying scene" << endl;
    if(iScene) {
        iScene->clear();
        delete(iScene);
    }
    cout << "destroying blackscene" << endl;
    if(iBlackScene) delete(iBlackScene);
    cout << "destroying timer" << endl;
    if(updateDisplayTimer) delete(updateDisplayTimer);
    if(BufferData) delete[] BufferData;
}

bool Feedback::getFeedbackError()
{
    return fError;
}
