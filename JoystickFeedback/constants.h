#ifndef CONSTANTS_H
#define CONSTANTS_H

#include <vector>

#endif // CONSTANTS_H

extern int TARGET_NUMBER;
extern int TARGET_DISTANCE;
extern int TARGET_SIZE;
extern int BALL_SIZE;

extern int CONSTANT_DEV;
extern int RDM_MAX_DEV;
extern int RDM_STEP_DEV;

extern int RESTING_TIME;
extern int WHITE_SCREEN_TIME;
extern int CURSOR_MIN_TIME;
extern int CURSOR_MAX_TIME;
extern int READY_CUE_MIN_TIME;
extern int READY_CUE_MAX_TIME;
extern int TRIAL_MAX_TIME;

extern int DATA_RATE;

extern int BOARD_NUM;

extern std::vector<int> PossibleDevAngles;
extern std::vector<int> TargetList;
extern float JoyXCalib, JoyYCalib;
