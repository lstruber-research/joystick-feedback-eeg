#include "mainwindow.h"
#include "ui_mainwindow.h"


using namespace std;


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    // Read constants file
    map<string, int> gVar;
    bool constRead = true;
    ifstream constFile("constants.txt");
    if (constFile.is_open())
    {
        string VarName;
        while(constFile >> VarName)
        {
            if(!(constFile >> gVar[VarName]))
                constRead = false;
        }
        constFile.close();
    }
    else constRead = false;

    if(constRead){
        if(gVar.find("TARGET_NUMBER") == gVar.end()) constRead = false; else TARGET_NUMBER = gVar["TARGET_NUMBER"];
        if(gVar.find("TARGET_DISTANCE") == gVar.end()) constRead = false; else TARGET_DISTANCE = gVar["TARGET_DISTANCE"];
        if(gVar.find("TARGET_SIZE") == gVar.end()) constRead = false; else TARGET_SIZE = gVar["TARGET_SIZE"];
        if(gVar.find("BALL_SIZE") == gVar.end()) constRead = false; else BALL_SIZE = gVar["BALL_SIZE"];

        if(gVar.find("CONSTANT_DEV") == gVar.end()) constRead = false; else CONSTANT_DEV = gVar["CONSTANT_DEV"];
        if(gVar.find("RDM_MAX_DEV") == gVar.end()) constRead = false; else RDM_MAX_DEV = gVar["RDM_MAX_DEV"];
        if(gVar.find("RDM_STEP_DEV") == gVar.end()) constRead = false; else RDM_STEP_DEV = gVar["RDM_STEP_DEV"];

        if(gVar.find("CURSOR_MIN_TIME") == gVar.end()) constRead = false; else CURSOR_MIN_TIME = gVar["CURSOR_MIN_TIME"];
        if(gVar.find("CURSOR_MAX_TIME") == gVar.end()) constRead = false; else CURSOR_MAX_TIME = gVar["CURSOR_MAX_TIME"];
        if(gVar.find("READY_CUE_MIN_TIME") == gVar.end()) constRead = false; else READY_CUE_MIN_TIME = gVar["READY_CUE_MIN_TIME"];
        if(gVar.find("READY_CUE_MAX_TIME") == gVar.end()) constRead = false; else READY_CUE_MAX_TIME = gVar["READY_CUE_MAX_TIME"];
        if(gVar.find("TRIAL_MAX_TIME") == gVar.end()) constRead = false; else TRIAL_MAX_TIME = gVar["TRIAL_MAX_TIME"];

        if(gVar.find("DATA_RATE") == gVar.end()) constRead = false; else DATA_RATE = gVar["DATA_RATE"];

        if(gVar.find("WHITE_SCREEN_TIME") == gVar.end()) constRead = false; else WHITE_SCREEN_TIME = gVar["WHITE_SCREEN_TIME"];

        // Compute possibles deviation angles from constants
        PossibleDevAngles.clear();
        int NbStepsDev = RDM_MAX_DEV/RDM_STEP_DEV;
        cout << "Possible deviation angles : ";
        for(int i = 0; i <= NbStepsDev; i++)
        {
            PossibleDevAngles.push_back(-RDM_MAX_DEV + i*RDM_STEP_DEV);
            cout << -RDM_MAX_DEV + i*RDM_STEP_DEV << ", ";
        }
        for(int i = NbStepsDev; i >= 0; i--)
        {
            PossibleDevAngles.push_back(RDM_MAX_DEV - i*RDM_STEP_DEV);
            cout << RDM_MAX_DEV - i*RDM_STEP_DEV << ", ";
        }
        cout << endl;

        // Create Target list
        TargetList.clear();
        for(int i = 0 ; i < TARGET_NUMBER; i++)
            TargetList.push_back(i);
    }

    if(!constRead)
    {
        QMessageBox::critical(this,tr("Powered by OptiMove inc."),tr("Impossible de lire les constantes"));
        exit(0);
    }

    // Initializing and configuring joystick
    BOARD_NUM = 0;
    float revision = (float)CURRENTREVNUM;
    int ULStat = cbDeclareRevision(&revision);
    cbErrHandling(PRINTALL, DONTSTOP);
    if(ULStat != 0)
    {
        // Error
        QMessageBox::critical(this,tr("Powered by OptiMove inc."),tr("Impossible d'initialiser le joystick"));
        // exit(0);
    }

    ULStat = cbDConfigBit(BOARD_NUM, AUXPORT, 0, DIGITALOUT);
    if(ULStat != 0)
    {
        // Error
        QMessageBox::critical(this,tr("Powered by OptiMove inc."),tr("Impossible d'initialiser le joystick"));
        exit(0);
    }
    ULStat = cbDBitOut(BOARD_NUM, AUXPORT, 0, 0);
    if(ULStat != 0)
    {
        // Error
        QMessageBox::critical(this,tr("Powered by OptiMove inc."),tr("Impossible d'initialiser le joystick"));
        exit(0);
    }
    unsigned Options = NOCONVERTDATA + SINGLEIO;
    long Rate = (long) DATA_RATE;
    long Count = 0.125 * Rate * 2; // Seconds * Samples/sec * NbChannels
    WORD* BufferData = new WORD[Count];
    ULStat = cbAInScan (BOARD_NUM, 0, 1, Count, &Rate, BIP5VOLTS, BufferData, Options);
    if(ULStat != 0)
    {
        // Error
        QMessageBox::critical(this,tr("Powered by OptiMove inc."),tr("Impossible d'initialiser le joystick"));
        exit(0);
    }

    short Status = 0;
    long CurCount, CurIndex;
    ULStat = cbGetStatus(BOARD_NUM, &Status, &CurCount, &CurIndex, AIFUNCTION);
    if(ULStat != 0 || Status != 0)
    {
        // Error
        QMessageBox::critical(this,tr("Powered by OptiMove inc."),tr("Impossible d'initialiser le joystick"));
        exit(0);
    }

    JoyXCalib = 0;
    JoyYCalib = 0;
    for(int i = 0; i < CurCount; i = i + 2)
    {
        JoyYCalib += ((float)BufferData[i]) * 10/65535 - 5;
        JoyXCalib += ((float)BufferData[i+1]) * 10/65535 - 5;
    }
    JoyYCalib /= CurCount/2;
    JoyXCalib /= CurCount/2;

    // Recover user folder
    char *user;
    size_t len;
    errno_t err = _dupenv_s(&user, &len, "USERPROFILE" );
    if(err || (!user))
    {
        QMessageBox::critical(this,tr("Powered by OptiMove inc."),tr("Impossible de trouver le dossier utilisateur"));
        exit(0);
    }

    // Get user name
    bool userDefined;
    fileNumberFam = 0;
    fileNumberJump = 0;
    fileNumberRot = 0;
    fileNumberRdmJump = 0;
    fileNumberRdmRot = 0;

    do {
        bool okReturned;
        userName = QInputDialog::getText(this, tr("Powered by OptiMove inc."), tr("Acronyme sujet :"), QLineEdit::Normal, trUtf8("Numéro + Initiales + Jour"), &okReturned, Qt::WindowFlags());
        if (!okReturned)
            exit(0);

        if(userName.isEmpty())
            userDefined = false;
        else {
            QRegExp StringMatcher("^[0-9]{1,2}[A-Z]{2,3}[0-9]{1,1}$", Qt::CaseInsensitive);
            if (StringMatcher.exactMatch(userName))
            {
                // Check if username folder exists
                strcpy_s(dataFolder, user);
                strcat_s(dataFolder, "\\Documents\\Manips");
                if(!QDir(dataFolder).exists())
                    QDir().mkdir(dataFolder);
                strcat_s(dataFolder, "\\JoystickFeedback\\");
                if(!QDir(dataFolder).exists())
                    QDir().mkdir(dataFolder);
                strcat_s(dataFolder, userName.toStdString().c_str());
                if(!QDir(dataFolder).exists())
                {
                    QDir().mkdir(dataFolder);
                    userDefined = true;
                }
                else
                {
                    // it already exist, ask if delete
                    QMessageBox existBox(QMessageBox::Question,tr("Powered by OptiMove inc."),trUtf8("Le sujet ") + userName + trUtf8(" semble déjà exister dans la base de données. Voulez-vous supprimer les données existantes ou continuer ?"), QMessageBox::Yes | QMessageBox::No | QMessageBox::Cancel);
                    existBox.setButtonText(QMessageBox::Yes, trUtf8("Supprimer"));
                    existBox.setButtonText(QMessageBox::No, trUtf8("Continuer"));
                    existBox.setButtonText(QMessageBox::Cancel, trUtf8("Annuler"));

                    int r = existBox.exec();
                    if(r == QMessageBox::Yes)
                    {
                        // it already exist, ask if delete
                        QMessageBox deletebox(QMessageBox::Question,tr("Powered by OptiMove inc."),trUtf8("Etes vous vraiment sûr de vouloir supprimer TOUTES les données du sujet ") + userName + trUtf8(" ?\nCette action est irréversible !"), QMessageBox::Yes | QMessageBox::No);
                        deletebox.setButtonText(QMessageBox::Yes, trUtf8("Oui"));
                        deletebox.setButtonText(QMessageBox::No, trUtf8("Non"));
                        int r = deletebox.exec();
                        if(r == QMessageBox::No)
                        {
                            userDefined = false;
                        }
                        else
                        {
                            QDir dir(dataFolder);
                            dir.setNameFilters(QStringList() << "*.*");
                            dir.setFilter(QDir::Files);
                            foreach(QString dirFile, dir.entryList())
                                dir.remove(dirFile);
                            userDefined = true;
                        }
                    }
                    else if(r == QMessageBox::No)
                    {
                        // check how many blocs are available
                        userDefined = true;
                        QDir dir(dataFolder);
                        dir.setNameFilters(QStringList() << "RAW_F_*.csv");
                        dir.setFilter(QDir::Files);
                        int nbFilesFam = dir.count();
                        for(int i = 0; i < nbFilesFam; i++)
                        {
                            QString fileName(QString("RAW_F_") + QString::number(i+1) + QString("_") + userName + QString(".csv"));
                            cout << fileName.toStdString().c_str() << endl;
                            QFileInfo checkFile(QString(dataFolder) + QString("\\") + fileName);
                            cout << checkFile.absoluteFilePath().toStdString().c_str() << endl;
                            if(!checkFile.exists() || !checkFile.isFile())
                            {
                                QMessageBox::critical(this,tr("Powered by OptiMove inc."),trUtf8("Erreur lors de la lecture des données existantes"));
                                userDefined = false;
                                break;
                            }
                        }
                        fileNumberFam = nbFilesFam;

                        // check how many blocs are available
                        userDefined = true;
                        dir.setNameFilters(QStringList() << "RAW_J_*.csv");
                        dir.setFilter(QDir::Files);
                        int nbFilesJump = dir.count();
                        for(int i = 0; i < nbFilesJump; i++)
                        {
                            QString fileName(QString("RAW_J_") + QString::number(i+1) + QString("_") + userName + QString(".csv"));
                            cout << fileName.toStdString().c_str() << endl;
                            QFileInfo checkFile(QString(dataFolder) + QString("\\") + fileName);
                            cout << checkFile.absoluteFilePath().toStdString().c_str() << endl;
                            if(!checkFile.exists() || !checkFile.isFile())
                            {
                                QMessageBox::critical(this,tr("Powered by OptiMove inc."),trUtf8("Erreur lors de la lecture des données existantes"));
                                userDefined = false;
                                break;
                            }
                        }
                        fileNumberJump = nbFilesJump;

                        // check how many blocs are available
                        userDefined = true;
                        dir.setNameFilters(QStringList() << "RAW_R_*.csv");
                        dir.setFilter(QDir::Files);
                        int nbFilesRot = dir.count();
                        for(int i = 0; i < nbFilesRot; i++)
                        {
                            QString fileName(QString("RAW_R_") + QString::number(i+1) + QString("_") + userName + QString(".csv"));
                            cout << fileName.toStdString().c_str() << endl;
                            QFileInfo checkFile(QString(dataFolder) + QString("\\") + fileName);
                            cout << checkFile.absoluteFilePath().toStdString().c_str() << endl;
                            if(!checkFile.exists() || !checkFile.isFile())
                            {
                                QMessageBox::critical(this,tr("Powered by OptiMove inc."),trUtf8("Erreur lors de la lecture des données existantes"));
                                userDefined = false;
                                break;
                            }
                        }
                        fileNumberRot = nbFilesRot;

                        // check how many blocs are available
                        userDefined = true;
                        dir.setNameFilters(QStringList() << "RAW_RJ_*.csv");
                        dir.setFilter(QDir::Files);
                        int nbFilesRdmJump = dir.count();
                        for(int i = 0; i < nbFilesRdmJump; i++)
                        {
                            QString fileName(QString("RAW_RJ_") + QString::number(i+1) + QString("_") + userName + QString(".csv"));
                            cout << fileName.toStdString().c_str() << endl;
                            QFileInfo checkFile(QString(dataFolder) + QString("\\") + fileName);
                            cout << checkFile.absoluteFilePath().toStdString().c_str() << endl;
                            if(!checkFile.exists() || !checkFile.isFile())
                            {
                                QMessageBox::critical(this,tr("Powered by OptiMove inc."),trUtf8("Erreur lors de la lecture des données existantes"));
                                userDefined = false;
                                break;
                            }
                        }
                        fileNumberRdmJump = nbFilesRdmJump;

                        // check how many blocs are available
                        userDefined = true;
                        dir.setNameFilters(QStringList() << "RAW_RR_*.csv");
                        dir.setFilter(QDir::Files);
                        int nbFilesRdmRot = dir.count();
                        for(int i = 0; i < nbFilesRdmRot; i++)
                        {
                            QString fileName(QString("RAW_RR_") + QString::number(i+1) + QString("_") + userName + QString(".csv"));
                            cout << fileName.toStdString().c_str() << endl;
                            QFileInfo checkFile(QString(dataFolder) + QString("\\") + fileName);
                            cout << checkFile.absoluteFilePath().toStdString().c_str() << endl;
                            if(!checkFile.exists() || !checkFile.isFile())
                            {
                                QMessageBox::critical(this,tr("Powered by OptiMove inc."),trUtf8("Erreur lors de la lecture des données existantes"));
                                userDefined = false;
                                break;
                            }
                        }
                        fileNumberRdmRot = nbFilesRdmRot;
                    }
                    else
                        userDefined = false;

                }
            }
            else {
                userDefined = false;
            }
        }
    } while(!userDefined);

    trialNumber = 0;

    switchScreen(0);
    iPhase = 0;
    iTrigRadius = 0;
    ui->statusBar->showMessage(userName,0);
}

MainWindow::~MainWindow()
{
    //feedback.wait();
    delete ui;
}

void MainWindow::on_StartButton_clicked()
{
    switchScreen(1);
    updateStatusBar();

    switch(iPhase)
    {
        case 0 :
            dispFeedback = new Feedback(*(ui->FeedbackView), dataFolder, userName, fileNumberFam, iPhase, iTrigRadius);
            break;
        case 1 :
            dispFeedback = new Feedback(*(ui->FeedbackView), dataFolder, userName, fileNumberJump, iPhase, iTrigRadius);
            break;
        case 2 :
            dispFeedback = new Feedback(*(ui->FeedbackView), dataFolder, userName, fileNumberRot, iPhase, iTrigRadius);
            break;
        case 3 :
            dispFeedback = new Feedback(*(ui->FeedbackView), dataFolder, userName, fileNumberRdmJump, iPhase, iTrigRadius);
            break;
        case 4 :
            dispFeedback = new Feedback(*(ui->FeedbackView), dataFolder, userName, fileNumberRdmRot, iPhase, iTrigRadius);
            break;
    }

    if(dispFeedback->isInitCanceled())
    {
        if(dispFeedback) delete(dispFeedback);
        return;
    }
    if(dispFeedback->getFeedbackError()) {
        QMessageBox::critical(this,tr("Powered by OptiMove inc."),tr("Impossible de dialoguer avec la carte MC"));
        if(dispFeedback) delete(dispFeedback);
        return;
    }

    EnableButtons(0);
    int nbTrials = this->ui->NbEssaisSpineBox->value();
    threadFeedback = new FeedbackThread(iPhase, nbTrials);
    connect(threadFeedback, SIGNAL(newTrial(int,double)), dispFeedback, SLOT(createScene(int,double)));
    connect(threadFeedback, SIGNAL(doSetCursor()), dispFeedback, SLOT(setCursor()));
    connect(threadFeedback, SIGNAL(doSetReadyCue()), dispFeedback, SLOT(setReadyCue()));
    connect(threadFeedback, SIGNAL(startTrial()), dispFeedback, SLOT(startPolling()));
    connect(threadFeedback, SIGNAL(doUpdateBallPos(float,float)), dispFeedback, SLOT(setBallPos(float,float)));
    connect(threadFeedback, SIGNAL(endTrial(bool)), dispFeedback, SLOT(stopTrial(bool)));
    connect(threadFeedback, SIGNAL(endTrial(bool)), this, SLOT(trialFinished()));
    connect(threadFeedback, SIGNAL(interruptTrial()), dispFeedback, SLOT(trialInterrupted()));
    connect(dispFeedback, SIGNAL(targetReached()), threadFeedback, SLOT(onTarget()));
    connect(dispFeedback, SIGNAL(feedbackError()), threadFeedback, SLOT(onError()));
    connect(threadFeedback, SIGNAL(fbError()), this, SLOT(onError()));
    connect(threadFeedback, SIGNAL(finished()), this, SLOT(onFinished()));
    threadFeedback->start(); 
}

void MainWindow::switchScreen(int screen)
{
    // Get screen size
    QRect rec0 = QApplication::desktop()->screenGeometry(0);

    if((screen == 1) && (QApplication::desktop()->screenCount() > 1))
    {
        QRect rec = QApplication::desktop()->screenGeometry(screen);
        screenHeight = rec.height();
        screenWidth = rec.width();
        this->setGeometry(rec0.width(),0,screenWidth,screenHeight);
    }
    else
    {
        screenHeight = rec0.height();
        screenWidth = rec0.width();
        this->setGeometry(0,0,screenWidth,screenHeight);
    }

    // View
    ui->FeedbackView->resize(screenWidth, screenHeight-25);
    ui->FeedbackView->setSceneRect(-screenWidth/2,-(screenHeight-25)/2,screenWidth,screenHeight-25);
    ui->FeedbackView->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    ui->FeedbackView->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

    int startButtonWidth = ui->StartButton->width();
    int startButtonHeight = ui->StartButton->height();
    ui->StartButton->move((screenWidth - startButtonWidth)/2, (screenHeight-25-startButtonHeight)/2);

    int closeButtonWidth = ui->CloseButton->width();
    int closeButtonHeight = ui->CloseButton->height();
    ui->CloseButton->move(screenWidth - closeButtonWidth - 25, closeButtonHeight - 5);

    int backButtonWidth = ui->BackButton->width();
    int backButtonHeight = ui->BackButton->height();
    ui->BackButton->move(screenWidth - backButtonWidth - 25, backButtonHeight - 5);
    ui->BackButton->setVisible(false);
    ui->BackButton->setEnabled(false);

    int comboWidth = ui->comboBox->width();
    int comboHeight = ui->comboBox->height();
    int labelWidth = ui->NbEssaisLabel->width();
    int labelHeight = ui->NbEssaisLabel->height();
    int nbEssaisWidth = ui->NbEssaisSpineBox->width();
    int nbEssaisHeight = ui->NbEssaisSpineBox->height();
    ui->comboBox->move((screenWidth - comboWidth - labelWidth - nbEssaisWidth)/2, (screenHeight-25-comboHeight)/2 + startButtonHeight/2 + 50);
    ui->NbEssaisLabel->move((screenWidth - comboWidth - labelWidth - nbEssaisWidth)/2 + comboWidth + 10, (screenHeight-25-labelHeight)/2 + startButtonHeight/2 + 50);
    ui->NbEssaisSpineBox->move((screenWidth - comboWidth - labelWidth - nbEssaisWidth)/2 + comboWidth + 10 + labelWidth + 10, (screenHeight-25-nbEssaisHeight)/2 + startButtonHeight/2 + 50);
}

void MainWindow::updateStatusBar()
{
    switch(iPhase)
    {
        case 0 :
            ui->statusBar->showMessage(userName + QString(trUtf8("\t\t\t\t\t\tF")) + QString::number(fileNumberFam + 1) + QString(trUtf8("\t\t\t\t\t\tEssai n°")) + QString::number(trialNumber + 1), 0);
            break;
        case 1 :
            ui->statusBar->showMessage(userName + QString(trUtf8("\t\t\t\t\t\tJ")) + QString::number(fileNumberJump + 1) + QString(trUtf8("\t\t\t\t\t\tEssai n°")) + QString::number(trialNumber + 1), 0);
            break;
        case 2 :
            ui->statusBar->showMessage(userName + QString(trUtf8("\t\t\t\t\t\tR")) + QString::number(fileNumberRot + 1) + QString(trUtf8("\t\t\t\t\t\tEssai n°")) + QString::number(trialNumber + 1), 0);
            break;
        case 3 :
            ui->statusBar->showMessage(userName + QString(trUtf8("\t\t\t\t\t\tRJ")) + QString::number(fileNumberRdmJump + 1) + QString(trUtf8("\t\t\t\t\t\tEssai n°")) + QString::number(trialNumber + 1), 0);
            break;
        case 4 :
            ui->statusBar->showMessage(userName + QString(trUtf8("\t\t\t\t\t\tRR")) + QString::number(fileNumberRdmRot + 1) + QString(trUtf8("\t\t\t\t\t\tEssai n°")) + QString::number(trialNumber + 1), 0);
            break;
    }
}

void MainWindow::trialFinished()
{
    trialNumber++;
    updateStatusBar();
}

void MainWindow::onFinished()
{
    if(dispFeedback) delete(dispFeedback);
    EnableButtons(1);
    switchScreen(0);
    trialNumber = 0;

    // Selection auto de la phase suivante
    switch(iPhase)
    {
        case 0 :
            fileNumberFam++;
            break;
        case 1 :
            fileNumberJump++;
            break;
        case 2 :
            fileNumberRot++;
            break;
        case 3 :
            fileNumberRdmJump++;
            break;
        case 4 :
            fileNumberRdmRot++;
            break;
    }

    ui->comboBox->setCurrentIndex((iPhase+1)%5);
    ui->statusBar->showMessage(userName,0);
}

void MainWindow::on_CloseButton_clicked()
{
    this->close();
}

void MainWindow::EnableButtons(bool enabled)
{
    ui->StartButton->setEnabled(enabled);
    ui->StartButton->setVisible(enabled);
    ui->CloseButton->setEnabled(enabled);
    ui->CloseButton->setVisible(enabled);
    ui->comboBox->setEnabled(enabled);
    ui->comboBox->setVisible(enabled);
    ui->NbEssaisLabel->setEnabled(enabled);
    ui->NbEssaisLabel->setVisible(enabled);
    ui->NbEssaisSpineBox->setEnabled(enabled);
    ui->NbEssaisSpineBox->setVisible(enabled);

    ui->BackButton->setVisible(!enabled);
    ui->BackButton->setEnabled(!enabled);
}

void MainWindow::on_BackButton_clicked()
{
    threadFeedback->interrupt();
}

void MainWindow::on_comboBox_currentIndexChanged(int index)
{
    iPhase = index;
    switch(iPhase)
    {
        case 0 :
            iTrigRadius = 0;
            ui->NbEssaisLabel->setText("NB Essais");
            ui->NbEssaisSpineBox->setValue(80);
            ui->NbEssaisSpineBox->setMaximum(999);
            break;
        case 1 :
            iTrigRadius = TARGET_DISTANCE/3;
            ui->NbEssaisLabel->setText("NB Essais");
            ui->NbEssaisSpineBox->setValue(80);
            ui->NbEssaisSpineBox->setMaximum(999);
            break;
        case 2 :
            iTrigRadius = 0;
            ui->NbEssaisLabel->setText("NB Essais");
            ui->NbEssaisSpineBox->setValue(80);
            ui->NbEssaisSpineBox->setMaximum(999);
            break;
        case 3 :
            iTrigRadius = TARGET_DISTANCE/3;
            ui->NbEssaisLabel->setText("n");
            ui->NbEssaisSpineBox->setValue(2);
            ui->NbEssaisSpineBox->setMaximum(TARGET_NUMBER);
            break;
        case 4 :
            iTrigRadius = 0;
            ui->NbEssaisLabel->setText("n");
            ui->NbEssaisSpineBox->setValue(2);
            ui->NbEssaisSpineBox->setMaximum(TARGET_NUMBER);
            break;
    }
}

void MainWindow::onError()
{
    QMessageBox::critical(this,tr("Powered by OptiMove inc."),tr("Impossible de dialoguer avec la carte MC"));
}
