#include "feedbackthread.h"

using namespace std;

FeedbackThread::FeedbackThread(int phase, int n) : iPhase(phase), N(n)
{
    switch(iPhase)
    {
        case 0 : case 1 : case 2 :
            NbTrials = N;
            break;
        case 3 : case 4 :
            NbTrials = TARGET_NUMBER*PossibleDevAngles.size() + N*PossibleDevAngles.size();
            break;
    }
    DevAngleVector.clear();
    TargetsVector.clear();
    stopRequest = false;
}

void FeedbackThread::run()
{
    srand(time(NULL));
    cout << "Extract sequence" << endl;
    extractAngleSequence();
    cout << "Extracted" << endl;

    feedbackError = false;
    //bool eventLaunched = false;
    for(int iTrial = 0; iTrial < NbTrials; iTrial++)
    {
        targetReached = false;
        // Draw targets in QPainter
        emit newTrial(TargetsVector[iTrial], DevAngleVector[iTrial]);
        this->msleep(WHITE_SCREEN_TIME);

        emit doSetCursor();
        int setCursorTime = rand()%(CURSOR_MAX_TIME + 1 - CURSOR_MIN_TIME) + CURSOR_MIN_TIME;
        cout << "CursorTime : " << setCursorTime << endl;
        this->msleep(setCursorTime);

        emit doSetReadyCue();
        int readyCueTime = rand()%(READY_CUE_MAX_TIME + 1 - READY_CUE_MIN_TIME) + READY_CUE_MIN_TIME;
        cout << "ReadyCueTime : " << readyCueTime << endl;
        this->msleep(readyCueTime);

        cout << "Start new trial" << endl;

        emit startTrial();

        double startTime = ((double) clock()/CLOCKS_PER_SEC)*1000;
        double currentTime = startTime;
        cout << "Feedback Error : " << feedbackError << endl;
        while((currentTime - startTime < TRIAL_MAX_TIME) && !targetReached && !feedbackError){
            currentTime = ((double) clock()/CLOCKS_PER_SEC)*1000;
            // cout << "times : " << startTime << "\t" << eventTime << "\t" << currentTime << "\t" << endl;
            // Uncomment for a time launched event
            /*if(((currentTime - startTime) >= eventTime) && !eventLaunched) {
                cout << "event" << endl;
                eventLaunched = true;
                emit intermediateEvent(currentTime - startTime);
            }*/
            if(stopRequest)
            {
                emit interruptTrial();
                return;
            }
        }
        if(feedbackError)
        {
            cout << "Feedback Error : " << feedbackError << endl;
            emit fbError();
            break;
        }
        this->msleep(100);
        //eventLaunched = false;
        emit endTrial(targetReached); // boolean 0 = timeout, 1 = target reached
    }
}

void FeedbackThread::interrupt()
{
    stopRequest = true;
}

void FeedbackThread::onTarget()
{
    targetReached = true;
}

void FeedbackThread::onError()
{
    feedbackError = true;
}

void FeedbackThread::extractAngleSequence()
{
    switch(iPhase) {
        case 0:
            DevAngleVector.resize(NbTrials,0);
            TargetsVector.resize(NbTrials);
            for(int i = 0; i < NbTrials; i++)
                TargetsVector[i] = rand() % TARGET_NUMBER;
            break;
        case 1: case 2:
            DevAngleVector.resize(NbTrials,(double) CONSTANT_DEV * PI/180);
            TargetsVector.resize(NbTrials);
            for(int i = 0; i < NbTrials; i++)
                TargetsVector[i] = rand() % TARGET_NUMBER;
            break;
        case 3: case 4:
            vector<double> IndexVector;
            vector<double> OrderedDevAngleVector;
            vector<double> OrderedTargetsVector;
            OrderedDevAngleVector.resize(NbTrials);
            OrderedTargetsVector.resize(NbTrials);
            IndexVector.resize(NbTrials);

            int NbAngles = PossibleDevAngles.size();
            for(int i = 0; i < TARGET_NUMBER; i++)
            {
                for(int j = 0; j < NbAngles; j++)
                {
                    OrderedDevAngleVector[j + i*NbAngles] = PossibleDevAngles[j];
                    OrderedTargetsVector[j + i*NbAngles] = i;
                    IndexVector[j + i*NbAngles] = j + i*NbAngles;
                }
            }

            for(int i = 0; i < N; i++)
            {
                for(int j = 0; j < NbAngles; j++)
                {
                    OrderedDevAngleVector[j + (TARGET_NUMBER + i)*NbAngles] = PossibleDevAngles[j];
                    OrderedTargetsVector[j + (TARGET_NUMBER + i)*NbAngles] = rand() % TARGET_NUMBER;
                    IndexVector[j + (TARGET_NUMBER + i)*NbAngles] = j + (TARGET_NUMBER + i)*NbAngles;
                }
            }

            std::random_shuffle(IndexVector.begin(), IndexVector.end());

            DevAngleVector.resize(NbTrials);
            TargetsVector.resize(NbTrials);

            for(int k = 0; k < NbTrials; k++)
            {
                DevAngleVector[k] = OrderedDevAngleVector[IndexVector[k]]*PI/180;
                TargetsVector[k] = OrderedTargetsVector[IndexVector[k]];
            }
            break;
    }
}
