#ifndef FEEDBACKTHREAD_H
#define FEEDBACKTHREAD_H

#include <iostream>
#include <QThread>
#include <QTimer>
#include <QEventLoop>
#include <QGraphicsView>
#include "feedback.h"

class FeedbackThread : public QThread
{
    Q_OBJECT
public:
    FeedbackThread(int phase, int nbTrials);
    void interrupt();

signals:
    void newTrial(int target, double deviation);
    void doSetCursor();
    void doSetReadyCue();
    void startTrial();
    void doUpdateBallPos(float x, float y);
    void endTrial(bool reached);
    void interruptTrial();
    // void intermediateEvent(double time);
    void fbError();
    void crossScreen();

public slots:
    void onTarget();
    void onError();

private:
    void run();
    void extractAngleSequence();
    bool targetReached;
    bool feedbackError;
    vector<double> DevAngleVector;
    vector<double> TargetsVector;
    int N, NbTrials;
    int iPhase;
    bool stopRequest;
};

#endif // FEEDBACKTHREAD_H
