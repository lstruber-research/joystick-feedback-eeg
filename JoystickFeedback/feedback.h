#ifndef FEEDBACK_H
#define FEEDBACK_H

#include <QObject>
#include <QGraphicsScene>
#include <QGraphicsItem>
#include <QDebug>
#include <QEvent>
#include <QKeyEvent>
#include <QGraphicsView>
#include "constants.h"
#include "time.h"
#include <QTimer>
#include <QDir>
#include <QMessageBox>
#include <QString>
#include <iostream>
#include <fstream>
#include "cbw.h"

#define PI   3.14159265358979323846

class QGraphicsItem;

using namespace std;
class Feedback : public QObject
{
    Q_OBJECT
public:
    explicit Feedback(QGraphicsView &view, char* dataFolder, QString username, int fileNumber, int phase, int trigRadius);
    ~Feedback();
    int BalltoTargetDist(float ballX, float ballY);
    bool getFeedbackError();
    bool isInitCanceled();
    void startEvent();

public slots :
    void createScene(int TargetNum, double Angle);
    void setCursor();
    void setReadyCue();
    void setBallPos(float x, float y);
    void startPolling();
    void updateDisplay();
    void stopTrial(bool isReached);
    void trialInterrupted();

signals:
    void targetReached();
    void feedbackError();

private:
    QGraphicsView &iView;
    QGraphicsScene *iScene, *iBlackScene;
    QGraphicsEllipseItem *iBall;
    vector<QGraphicsEllipseItem*> iTargets;
    int iTargetNum;
    double dAngle;
    double dStartAngularDev;
    double dRealAngularDev;
    int startTargetXCenter, startTargetYCenter;
    int realTargetXCenter, realTargetYCenter;
    vector<int> ballXPos, ballYPos, TrigVector;
    WORD*  BufferData;
    QTimer* updateDisplayTimer;
    int targetTimes;
    ofstream dataFile;
    char fileName[100];
    int iPhase;
    bool fError;
    bool initCanceled;
    float JoystickX_old, JoystickY_old, BallXCenter_old, BallYCenter_old;
    long Rate;
    long Count;
    bool eventLaunched;
    int iTrigRadius;
    bool sceneOnceCreated;
};

#endif // FEEDBACK_H
