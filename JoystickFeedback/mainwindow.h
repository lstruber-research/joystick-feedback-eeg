#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QInputDialog>
#include <QMessageBox>
#include <QDesktopWidget>
#include <QPainter>
#include <QSize>
#include <QDir>
#include <iostream>
#include <feedback.h>
#include <QGraphicsScene>
#include <QGraphicsRectItem>
#include <QGraphicsEllipseItem>
#include "feedback.h"
#include <QPen>
#include <QResizeEvent>
#include <QDebug>
#include <QThread>
#include "feedbackthread.h"
#include <map>
#include <fstream>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_StartButton_clicked();
    void onFinished();
    void onError();
    //void onCrossScreen();
    void trialFinished();
    void on_CloseButton_clicked();
    void on_BackButton_clicked();
    void on_comboBox_currentIndexChanged(int index);

private:
    Ui::MainWindow *ui;
    void updateStatusBar();
    void EnableButtons(bool enabled);
    void switchScreen(int screen);
    QString userName;
    int screenHeight;
    int screenWidth;
    QGraphicsScene *iScene;
    Feedback *dispFeedback;
    FeedbackThread *threadFeedback;
    int trialNumber;
    char dataFolder[100];
    int iPhase; // 0 = FAM, 1 = JUMP, 2 = ADAPTATION
    int iTrigRadius;
    int fileNumberFam,fileNumberJump,fileNumberRot,fileNumberRdmJump,fileNumberRdmRot;

};

#endif // MAINWINDOW_H
