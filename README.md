# Joystick Feedback EEG
Joystick Feedback EEG is a C++/Qt project to display feedback (real or visually distorded) from acquisition of joystick data.

## Installation
This project was coded on QtCreator 3.8.0 using Qt 4.8.6

## License
[MIT](https://choosealicense.com/licenses/mit/)
